package co.com.pruebas.unitarias;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class VowelCounterTest {
	
	@ParameterizedTest
	@CsvSource({"hola,2", "programar, 3", "perro, 5"})
	public void contarTest(String palabra, int vocales) {
		Assertions.assertEquals(VowelCounter.contar(palabra), vocales);
	}

}
