package co.com.pruebas.unitarias;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CalculadoraTest {

	@Mock
	private Calculadora calculadoraMock;

	@InjectMocks
	private Calculadora calculadora;

	@Captor
	ArgumentCaptor<Integer> dividendoCaptor;

	@Captor
	ArgumentCaptor<Integer> divisorCaptor;

	@Captor
	ArgumentCaptor<Integer> sumCaptor2;

	@Captor
	ArgumentCaptor<Integer> sumCaptor1;

	@BeforeEach
	public void setup() {
		calculadora = new Calculadora();
	}

	@Test
	public void testAdd() {
		assertEquals(5, calculadora.add(2, 3));
		assertEquals(-1, calculadora.add(-2, 1));
		assertEquals(0, calculadora.add(0, 0));
	}

	@Test
	public void testSubtract() {
		assertEquals(3, calculadora.subtract(5, 2));
		assertEquals(-4, calculadora.subtract(0, 4));
		assertEquals(0, calculadora.subtract(10, 10));
	}

	@Test
	public void testMultiply() {
		assertEquals(10, calculadora.multiply(2, 5));
		assertEquals(0, calculadora.multiply(0, 100));
		assertEquals(-15, calculadora.multiply(3, -5));
	}

	@Test
	public void testDivide() {
		assertEquals(3, calculadora.divide(9, 3));
		assertEquals(0, calculadora.divide(0, 10));
		assertEquals(-4, calculadora.divide(20, -5));
		assertEquals(3, calculadora.divide(100, 33), 0.3);
		try {
			calculadora.divide(10, 0);
			fail("No se puede dividir por cero");
		} catch (ArithmeticException e) {
		}
	}

	@Test
	public void testDivideByZero() {
		int dividendo = 10;
		int divisor = 0;

		Mockito.when(calculadoraMock.divide(dividendoCaptor.capture(), divisorCaptor.capture()))
				.thenThrow(new IllegalArgumentException("No se puede dividir por cero"));

		Assertions.assertThrows(IllegalArgumentException.class, () -> calculadoraMock.divide(dividendo, divisor));

		Mockito.verify(calculadoraMock).divide(dividendoCaptor.capture(), divisorCaptor.capture());

		Assertions.assertEquals(dividendo, dividendoCaptor.getValue());
		Assertions.assertEquals(divisor, divisorCaptor.getValue());

	}

	@Test
	public void testMultiplyCapture() {
		int resultado = 10;
		int param1 = 8;
		int param2 = 2;

		Mockito.when(calculadoraMock.add(sumCaptor1.capture(), sumCaptor2.capture())).thenReturn(resultado);
		Assertions.assertEquals(resultado, calculadoraMock.add(param1, param2));

		Mockito.verify(calculadoraMock).add(sumCaptor1.capture(), sumCaptor2.capture());

		Assertions.assertEquals(param1, sumCaptor1.getValue());
		Assertions.assertEquals(param2, sumCaptor2.getValue());

	}

	@TestFactory
	public List<DynamicTest> testResultados(){
		int[] reusltados  = {10, 20, 40};
		
		List<DynamicTest> tests = new ArrayList<>();
		
		for (int numero: reusltados) {
			String  testName = "prueba dinamica resultado= " + String.valueOf(numero);
			DynamicTest test = DynamicTest.dynamicTest( testName, ()-> {
				assertEquals(numero, calculadora.multiply(2, 5));
			});
			tests.add(test);
		}
		
		return tests;
		
	}

}
