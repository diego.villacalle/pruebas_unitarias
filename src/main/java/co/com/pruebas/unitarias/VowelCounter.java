package co.com.pruebas.unitarias;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VowelCounter {

	static int contar(String palabra) {
		Pattern pattern = Pattern.compile("[aeiouAEIOU]");
		Matcher matcher = pattern.matcher(palabra);

		int count = 0;
		while (matcher.find()) {
			count++;
		}
		return count;
	}

}
